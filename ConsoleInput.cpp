#include "ConsoleInput.h"

using namespace std;

// ==========================================================================================================
// ConsoleInput.cpp
// ----------------------------------------------------------------------------------------------------------
// Methods that help print and retrieve input from the user.
// Angela Gross
// ==========================================================================================================

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ==========================================================================================================
// Retrieves a line from the user
// ==========================================================================================================
string getLine()
{
    string result;
    getline(cin, result);
    return result;
}

// ==========================================================================================================
// Retrieves an integer from the user
// ==========================================================================================================
int getInt()
{
    while(true) // Read input until user enters valid data
    {
        stringstream converter;
        converter << getLine();
        
        // Try reading an int, continue if we succeeded. 
        int result;
        converter >> result;
        
        if(!converter.fail())
        {
            char remaining;
            converter >> remaining; // Check for stray input
            
            if(converter.fail()) // Couldn't read any more, so input is valid
                return result;
            else cout << "Unexpected character: " << remaining << endl;
        }
        
        else cout << "Please enter an integer." << endl;
        
        cout << "Retry: ";
    }
}

// ==========================================================================================================
// Retrieves a float from the user
// ==========================================================================================================
float getFloat()
{
    stringstream converter;
    converter << getLine();

    // Try reading a float, continue if we succeeded.
    float result;
    converter >> result;

    if(!converter.fail())
    {
        char remaining;
        converter >> remaining; // Check for stray input

        if(converter.fail()) // Couldn't read any more, so input is valid
            return result;
        else cout << "Unexpected character: " << remaining << endl;
    }

    else cout << "Please enter a float." << endl;

    cout << "Retry: ";
}

// ==========================================================================================================
// Prints out a line
// ==========================================================================================================
void printLine(string myString)
{
    cout << myString << "\n";
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
