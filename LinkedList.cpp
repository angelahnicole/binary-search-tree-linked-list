#include "LinkedList.h"

// ==========================================================================================================
// LinkedList.cpp
// ----------------------------------------------------------------------------------------------------------
// Implements simple methods for a doubly linked list with head and tail sentinel nodes. Also is able to 
// read in a BST and load it in by index.
// ----------------------------------------------------------------------------------------------------------
// Angela Gross
// Unit 3
// ==========================================================================================================

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // CONSTRUCTOR and DESTRUCTOR

    template <typename T> LinkedList<T>::LinkedList() 
    { 
        // Initialize head and tail pointers by using sentinel nodes
        T tempData;
        head = createNode(tempData, 0, 0, -1);
        tail = createNode(tempData, 0, head, -1);
        head->setNext(tail);

        // Initialize list size
        size = 0;
    }

    template <typename T> LinkedList<T>::~LinkedList()
    {
        clearList();
        delete head;
        delete tail;
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    // =====================================================================================================
    // INSERTHEAD()
    // -----------------------------------------------------------------------------------------------------
    // Insert data at the beginning of the list
    // =====================================================================================================
    template <typename T> Node<T>* LinkedList<T>::insertHead(const T& data, const int& index)
    {
        // Get first node
        Node<T>* firstNodePtr = head->getNext();

        // New node has its next to be the first node and its prev to be the head sentinel
        Node<T>* newNodePtr = createNode(data, firstNodePtr, head, index);

        // Update first node's prev and head's next to be the new node
        firstNodePtr->setPrev(newNodePtr);
        head->setNext(newNodePtr);

        // Increase list's size
        size++;

        return newNodePtr;
    }

    // =====================================================================================================
    // INSERTTAIL()
    // -----------------------------------------------------------------------------------------------------
    // Insert data at the end of the list
    // =====================================================================================================
    template <typename T> Node<T>* LinkedList<T>::insertTail(const T& data, const int& index)
    {
        // Get last node
        Node<T>* lastNodePtr = tail->getPrev();

        // New node has its next to be the tail sentinel and its prev the last node
        Node<T>* newNodePtr = createNode(data, tail, lastNodePtr, index);

        // Update last node's next and tail's prev to be the new node
        lastNodePtr->setNext(newNodePtr);
        tail->setPrev(newNodePtr);

        // Increase list's size
        size++;

        return newNodePtr;
    }

    // =====================================================================================================
    // INSERTAFTER()
    // -----------------------------------------------------------------------------------------------------
    // Insert data after a specified node
    // =====================================================================================================
    template <typename T> Node<T>* LinkedList<T>::insertAfter(Node<T>* currPtr, const T& data, const int& index)
    {
        // It's okay if the previous is 0 since it could be the head
        if(currPtr == 0)
        {
            cout << "Can't insert after an invalid node. \n";
            exit(1);  
        }

        // Based on the current pointer, get its next
        Node<T>* nextPtr = currPtr->getNext();

        // Found node (or not) - create it
        Node<T>* newNextPtr = 0;
        Node<T>* newPrevPtr = 0;
        Node<T>* newNodePtr = 0;

        // Iterated all the way through and is being inserted at the tail
        if(nextPtr == 0)
        {
            // Make the new node
            newNextPtr = tail;
            newPrevPtr = tail->getPrev();
            newNodePtr = createNode(data, newNextPtr, newPrevPtr, index);
            
            // Update last node's next
            Node<T>* lastNode = tail->getPrev();
            lastNode->setNext(newNodePtr);
            
            // Update tail's last
            tail->setPrev(newNodePtr);
        }
        else
        {
            // Create the new node
            newNextPtr = nextPtr;
            newPrevPtr = currPtr;
            newNodePtr = createNode(data, newNextPtr, newPrevPtr, index);
            
            // Set current node's next 
            currPtr->setNext(newNodePtr);
            
            // Set next node's last
            nextPtr->setPrev(newNodePtr);
        }

        // Increase list's size
        size++;

        return newNodePtr;
    }
    
    template <typename T> Node<T>* LinkedList<T>::insertByIndex(const T& data, const int& index)
    {       
        Node<T>* nextPtr = head->getNext();
        Node<T>* currPtr = head;
        Node<T>* newNodePtr = NULL;     
        bool isFound = false;
        
        if(!isEmpty())
        {
            // Find pointer that we'll insert after
            while( (!isFound) && (nextPtr != 0) )
            {
                if(nextPtr->getIndex() < index)
                {
                    currPtr = nextPtr;
                    nextPtr = currPtr->getNext();
                }
                else
                {
                    isFound = true;
                }
            }

            newNodePtr = insertAfter(currPtr, data, index);
        }
        else
        {
            newNodePtr = insertHead(data, index);
        }

        return newNodePtr;
    }

    // =====================================================================================================
    // FIND()
    // -----------------------------------------------------------------------------------------------------
    // Finds a node with the given data (if there are duplicates, then it will return the first one it 
    // encounters).
    // =====================================================================================================
    template <typename T> Node<T>* LinkedList<T>::find(const T& targetData)
    {
        Node<T>* nextPtr = head->getNext();
        Node<T>* currPtr = head;
        bool isFound = false;

        // Find target pointer
        while( (!isFound) && (nextPtr != 0) )
        {
            if(currPtr->getData() != targetData)
            {
                currPtr = nextPtr;
                nextPtr = currPtr->getNext();
            }
            else
            {
                isFound = true;
            }
        }

        return currPtr;
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // =====================================================================================================
    // DELETEHEAD()
    // -----------------------------------------------------------------------------------------------------
    // Delete the first item in the list
    // =====================================================================================================
    template <typename T> void LinkedList<T>::deleteHead()
    {
        if( isEmpty() )
        {
            cout << "Can't delete an item from an empty list! \n";
            exit(1);
        }

        // Set the second node's prev to the head and set head's next to the second node.
        Node<T>* firstNodePtr = head->getNext();
        Node<T>* secondNodePtr = firstNodePtr->getNext();
        head->setNext(secondNodePtr);
        secondNodePtr->setPrev(head);

        delete firstNodePtr;

        size--;
    }

    // =====================================================================================================
    // DELETETAIL()
    // -----------------------------------------------------------------------------------------------------
    // Delete the second item in the list
    // =====================================================================================================
    template <typename T> void LinkedList<T>::deleteTail()
    {
        if( isEmpty() )
        {
            cout << "Can't delete an item from an empty list! \n";
            exit(1);
        }

        // Set the second to last node's next to the tail and set tail's prev to the second to last node
        Node<T>* lastNodePtr = tail->getPrev();
        Node<T>* secondLastNodePtr = lastNodePtr->getPrev();
        tail->setPrev(secondLastNodePtr);
        secondLastNodePtr->setNext(tail);

        delete lastNodePtr;

        size--;
    }

    // =====================================================================================================
    // CLEARLIST()
    // -----------------------------------------------------------------------------------------------------
    // Delete all items in the list
    // =====================================================================================================
    template <typename T> void LinkedList<T>::clearList()
    {
        Node<T>* currNodePtr = head->getNext();
        Node<T>* tempNodePtr = 0;

        while(currNodePtr != 0)
        {
            tempNodePtr = currNodePtr;
            currNodePtr = currNodePtr->getNext();

            // sentinel node; don't want to delete it
            if(currNodePtr != 0)
                delete tempNodePtr;
        }

        // Clean up sentinel nodes and reset size
        head->setNext(tail);
        tail->setPrev(head);
        size = 0;
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // =====================================================================================================
    // CREATENODE()
    // -----------------------------------------------------------------------------------------------------
    // Create a new node while making sure that bad allocation hasn't occurred.
    // =====================================================================================================
    template <typename T> Node<T>* LinkedList<T>::createNode(const T& data, Node<T>* nextPtr, Node<T>* prevPtr, const int& index)
    {
        Node<T>* newNodePtr = 0;

        try
        {
            // Make the new node
            if(index >= 0)
                newNodePtr = new Node<T>(data, nextPtr, prevPtr, index);
            else
                newNodePtr = new Node<T>(data, nextPtr, prevPtr, size);
        }
        catch(int ex)
        {
            cout << "An exception occurred while making a new Node object. Exception # " << ex << '\n';
            exit(1);
        }

        return newNodePtr;
    }

    // =====================================================================================================
    // ISEMPTY()
    // -----------------------------------------------------------------------------------------------------
    // Returns whether or not the list is empty
    // =====================================================================================================
    template <typename T> bool LinkedList<T>::isEmpty()
    {
       return (size == 0); 
    }

    // =====================================================================================================
    // GETSIZE()
    // -----------------------------------------------------------------------------------------------------
    // Returns the list's total size
    // =====================================================================================================
    template <typename T> int LinkedList<T>::getSize()
    {
        return size;
    }

    // =====================================================================================================
    // PRINTLIST()
    // -----------------------------------------------------------------------------------------------------
    // Iterates through the whole list and prints the data (skipping the sentinel nodes)
    // =====================================================================================================
    template <typename T> void LinkedList<T>::printList()
    {
        if(!isEmpty())
        {
            Node<T>* currPtr = head->getNext(); // want to start on the first node
            Node<T>* nextPtr = currPtr->getNext();      

            while(nextPtr != 0)
            {
                // Get data and print it
                T data = currPtr->getData();     
                cout << data << "\t";

                // Move to the next item
                currPtr = nextPtr;
                nextPtr = currPtr->getNext();
            }

            cout << "\n";
        }
        else
        {
            cout << "List is empty. \n";
        }
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
