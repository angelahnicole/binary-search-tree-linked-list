#include <iostream>
#include <fstream> // For ifstream
#include <sstream>
#include <string>
#include <cmath>

using namespace std;

// ==========================================================================================================
// ConsoleInput.h
// ----------------------------------------------------------------------------------------------------------
// Header file for methods that help print and retrieve input from the user.
// Angela Gross
// ==========================================================================================================

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef CONSOLEINPUT_H
#define	CONSOLEINPUT_H

string getLine();
int getInt();
float getFloat();
void printLine(string);

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////



