#include "BST.h"

using namespace std;

// ==========================================================================================================
// BST.h
// ----------------------------------------------------------------------------------------------------------
// Basic class for a binary search tree.
// ----------------------------------------------------------------------------------------------------------
// Angela Gross
// Unit 6
// ==========================================================================================================

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// CONSTRUCTOR
template <typename T> BST<T>::BST()
{
    this->root = NULL;
    this->size = 0;
}

// DESTRUCTOR
template <typename T> BST<T>::~BST()
{
    clearTree();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T> void BST<T>::fillList(LinkedList<T>* myLinkedList)
{
    fillList(myLinkedList, this->root);
}

template <typename T> void BST<T>::fillList(LinkedList<T>* myLinkedList, Node<T>* current)
{
    if(current != NULL)
    {
        myLinkedList->insertByIndex(current->getData(), current->getIndex());
        fillList( myLinkedList, current->getLeft() );
        fillList( myLinkedList, current->getRight() );
    }
}

// ==========================================================================================================
// INSERT()
// ----------------------------------------------------------------------------------------------------------
// Traverses tree to figure out where in the tree the data needs to be inserted, and creates a node and 
// positions it appropriately.
// ==========================================================================================================
template <typename T> void BST<T>::insert(const T& data)
{
    Node<T>* current = this->root;
    Node<T>* parent = NULL;
    Node<T>* newNode = NULL;
    
    // Create our new node
    newNode = createNode(data, NULL, NULL);
    
    // Keep traversing until we hit the leaves
    while(current != NULL)
    {
        parent = current;
        
        // New data is less than parent, so move left
        if( data < current->getData() )
            current = parent->getLeft();
        // New data is greater than or equal to parent, so move right
        else
            current = parent->getRight();        
    }
    
    // First node to be inserted
    if(parent == NULL)
        this->root = newNode;
    // New data is less than parent, so move left
    else if(data < parent->getData() )
        parent->setLeft(newNode);
    // New data is greater than or equal to parent, so move right
    else
        parent->setRight(newNode);
    
    this->size++;  
}

// ==========================================================================================================
// FIND()
// ----------------------------------------------------------------------------------------------------------
// Traverse through tree to find the node with the desired data. (Entry point)
// ==========================================================================================================
template <typename T> Node<T>* BST<T>::find(const T& data)
{
    return find(data, this->root);
}


// ==========================================================================================================
// FIND()
// ----------------------------------------------------------------------------------------------------------
// Traverse through tree to find the node with the desired data.
// ==========================================================================================================
template <typename T> Node<T>* BST<T>::find(const T& data, Node<T>* current)
{
    // Reached a leaf; couldn't find it.
    if(current == NULL)
        return NULL;
    // Found the data!
    else if( data == current->getData() )
        return current;
    // Data is less than the current data, so move left
    else if( data < current->getData() )
        return find( data, current->getLeft() );
    // Data is greater than or equal to current data, so move right
    else
        return find( data, current->getRight() );
}

// ==========================================================================================================
// PRINTINORDER()
// ----------------------------------------------------------------------------------------------------------
// Since this is a BST, we'll use an inorder traversal to print the data. (Entry point)
// ==========================================================================================================
template <typename T> void BST<T>::printInOrder()
{
    printInOrder(this->root);
    cout << "\n";
}

// ==========================================================================================================
// PRINTINORDER()
// ----------------------------------------------------------------------------------------------------------
// Since this is a BST, we'll use an inorder traversal to print the data. 
// ==========================================================================================================
template <typename T> void BST<T>::printInOrder(Node<T>* current)
{
    if(current != NULL)
    {
        printInOrder( current->getLeft() );
        cout << current->getData() << "\t";
        printInOrder( current->getRight() );
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ==========================================================================================================
// CREATENODE()
// ----------------------------------------------------------------------------------------------------------
// Creates a new BST Node and makes sure that a bad memory allocation doesn't occur.
// ==========================================================================================================
template <typename T> Node<T>* BST<T>::createNode(const T& data, Node<T>* left, Node<T>* right)
{
    Node<T>* newNode = NULL;

    try
    {
        newNode = new Node<T>(data, left, right, size); // this works because size is incremented after
    }
    catch(int ex)
    {
        cout << "An exception occurred while making a new Node object. Exception # " << ex << '\n';
        exit(1);
    }

    return newNode;
}

// ==========================================================================================================
// DESTROYNODE()
// ----------------------------------------------------------------------------------------------------------
// Frees the memory that the node was taking up.
// ==========================================================================================================
template <typename T> void BST<T>::destroyNode(Node<T>* node)
{
    delete node;
    this->size--;
}

// ==========================================================================================================
// CLEARTREE()
// ----------------------------------------------------------------------------------------------------------
// Uses a post-order traversal (i.e. delete children before parents) to clear the tree so we don't leak 
// memory. (Entry point)
// ==========================================================================================================
template <typename T> void BST<T>::clearTree()
{
    clearTree(this->root);
}

// ==========================================================================================================
// CLEARTREE()
// ----------------------------------------------------------------------------------------------------------
// Uses a post-order traversal (i.e. delete children before parents) to clear the tree so we don't leak 
// memory.
// ==========================================================================================================
template <typename T> void BST<T>::clearTree(Node<T>* current)
{
    if(current != NULL)
    {
        clearTree( current->getLeft() );
        clearTree( current->getRight() );
        destroyNode(current);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ==========================================================================================================
// GETSIZE()
// ----------------------------------------------------------------------------------------------------------
// Returns the number of nodes in the BST.
// ==========================================================================================================
template <typename T> int BST<T>::getSize()
{
    return this->size;
}

// ==========================================================================================================
// ISEMPTY()
// ----------------------------------------------------------------------------------------------------------
// Returns true if the number of nodes in the BST is zero.
// ==========================================================================================================
template <typename T> bool BST<T>::isEmpty()
{
    return (this->size == 0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
