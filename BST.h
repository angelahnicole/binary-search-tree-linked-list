
#ifndef BST_H
#define	BST_H

#include <cstddef>
#include <iostream>
#include <stdlib.h>
#include "Node.hpp"
#include "LinkedList.h"

using namespace std;

// ==========================================================================================================
// BST.h
// ----------------------------------------------------------------------------------------------------------
// Header for a basic class for a binary search tree.
// ----------------------------------------------------------------------------------------------------------
// Angela Gross
// Unit 6
// ==========================================================================================================

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
class BST
{
    private:
        Node<T>* root;
        int size;
        void fillList(LinkedList<T>* myLinkedList, Node<T>* current);
        Node<T>* find(const T& data, Node<T>* current);
        void printInOrder(Node<T>* current);
        Node<T>* createNode(const T& data, Node<T>* left, Node<T>* right);
        void destroyNode(Node<T>* node);
        void clearTree();
        void clearTree(Node<T>* current);
    public:
        BST();
        ~BST();
        void fillList(LinkedList<T>* myLinkedList);
        void insert(const T& data);
        Node<T>* find(const T& data);
        void printInOrder();
        int getSize();
        bool isEmpty();
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif	// BST_H

