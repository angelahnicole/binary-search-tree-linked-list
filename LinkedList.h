#ifndef LINKEDLIST_H
#define	LINKEDLIST_H

#include "Node.hpp"
#include "ConsoleInput.h"
#include <stdlib.h>

// ==========================================================================================================
// LinkedList.h
// ----------------------------------------------------------------------------------------------------------
// Header file (which includes console input and node classes) that implements a doubly linked list with 
// head and tail sentinel nodes. Also is able to read in a BST and load it in by index.
// ----------------------------------------------------------------------------------------------------------
// Angela Gross
// Unit 3
// ==========================================================================================================

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
class LinkedList
{
    private:
        Node<T>* head, * tail;
        int size;
    public:
        LinkedList();
        ~LinkedList();
        Node<T>* insertHead(const T& data, const int& index);
        Node<T>* insertTail(const T& data, const int& index);
        Node<T>* insertAfter(Node<T>* currPtr, const T& data, const int& index);
        Node<T>* insertByIndex(const T& data, const int& index);
        Node<T>* find(const T& data);
        void deleteHead();
        void deleteTail();
        void clearList();
        Node<T>* createNode(const T& data, Node<T>* next, Node<T>* prev, const int& index);
        bool isEmpty();
        int getSize();
        void printList();
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif	// LINKEDLIST_H

