#include <cstdlib>
#include "BST.h"
#include "BST.cpp"
#include "LinkedList.h"
#include "LinkedList.cpp"
 #include <fstream> // For ifstream

using namespace std;

// ==========================================================================================================
// main.cpp
// ----------------------------------------------------------------------------------------------------------
// Tests out the methods of a BST that is able to fill a list in order of their indices (i.e. the order
// they were inserted).
// ----------------------------------------------------------------------------------------------------------
// Angela Gross
// Unit 6
// ==========================================================================================================

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void readFileIntoTree(BST<string>* myTree);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// MAIN METHOD

int main(int argc, char** argv)
{
    
    BST<string>* myTree = new BST<string>();
    LinkedList<string>* myLinkedList = new LinkedList<string>();

    // Read in data from file
    readFileIntoTree(myTree);

    // Print the tree
    // (Should be in alphabetical order)
    printLine("====================================================================");
    printLine("Tree In Order Print:");
    myTree->printInOrder();
    printLine("====================================================================");
    printLine("");
    
    // Have linked list read in tree
    myTree->fillList(myLinkedList);
    
    // Print the list
    // (Should be in original order)
    printLine("====================================================================");
    printLine("List Print:");
    myLinkedList->printList();
    printLine("====================================================================");
    printLine("");

    // Close up that memory!
    delete myTree;
    delete myLinkedList;

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// =========================================================================================================
// Reads file word by word and places each word into a BST.
// =========================================================================================================
void readFileIntoTree(BST<string>* myTree)
{
    ifstream input("gettysburg.dat");
    
    // Check if the file could be opened.
    if( !input.is_open() )
    {
        cerr << "Cannot open file!" << endl;
        exit(1);
    }
    
    while(true)
    {
        // Check if it's the end of the file
        if( input.eof() )
            break;
        
        // Read word
        string word;
        input >> word;
        
        // Insert into the tree
        myTree->insert(word);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

