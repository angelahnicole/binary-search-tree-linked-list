#ifndef NODE_HPP
#define	NODE_HPP

// ==========================================================================================================
// Node.hpp
// ----------------------------------------------------------------------------------------------------------
// Basic class for a generic node for both BST and Linked Lists. Includes a read-only index. 
// ----------------------------------------------------------------------------------------------------------
// Angela Gross
// Unit 6
// ==========================================================================================================

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
class Node
{
    private:
        T data;
        Node<T>* left;
        Node<T>* right;
        int index;
    public:
        Node(const T& newData, Node<T>* newLeft, Node<T>* newRight) : data(newData), left(newLeft), right(newRight){}
        Node(const T& newData, Node<T>* newLeft, Node<T>* newRight, int newIndex) : data(newData), left(newLeft), right(newRight), index(newIndex){}
        T getData() { return data; }
        Node<T>* getLeft() { return left; }
        Node<T>* getRight() { return right; }
        Node<T>* getNext() { return left; }
        Node<T>* getPrev() { return right; }
        int getIndex() { return index; } 
        void setData(const T& newData) { data = newData; }
        void setLeft(Node<T>* newLeft) { left = newLeft; }
        void setRight(Node<T>* newRight) { right = newRight; }
        void setNext(Node<T>* newNext) { left = newNext; }
        void setPrev(Node<T>* newPrev) { right = newPrev; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif	// NODE_HPP

