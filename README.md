# README #

This is a simple C++ program that demonstrates my knowledge of binary search trees and linked lists, and it was assigned by the following MOOC: [CS 107: C++ Programming](http://www.saylor.org/courses/cs107/)

The LinkedList and BST classes came together from two separate assignments, and they are linked in such a way that a BST can take in a LinkedList add its own nodes to the LinkedList in the order that the Nodes were added to the BST.

### Example ###

**File input:**

```
four score and seven years ago our fathers
```

**BST In Order Print:**
```
ago and fathers four our score seven years
```

**List Print:**
```
four score and seven years ago our fathers
```